import csv
from itertools import zip_longest
from pathlib import Path

import torch
from torch.utils.data import DataLoader
from tqdm import tqdm

from src.datasets import PandasPickleDataset
from src.models import Model

DATA_DIR = Path('data')


def grouper(iterable, n, fillvalue=None):
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


if __name__ == '__main__':
    torch.manual_seed(0)

    dataset = PandasPickleDataset(
        data_file=DATA_DIR / 'test_clean.pkl',
        feature_type_file=DATA_DIR / 'variables-type.json',
        normalization_params_file='data/train_clean-norm-params.json'
    )

    checkpoint = torch.load('lightning_logs/version_1/checkpoints/epoch=7.ckpt')

    model = Model(dataset)
    model.load_state_dict(checkpoint['state_dict'])
    model.eval()

    chunk_size = 2048

    with torch.no_grad(), open('submission_pt.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        writer.writerow(['TransactionID', 'isFraud'])

        for batch, ids in tqdm(
                iterable=zip(DataLoader(dataset, batch_size=chunk_size),
                             grouper(dataset.data.TransactionID, chunk_size)),
                desc='Making predictions',
                total=len(dataset) // chunk_size
        ):
            batch_pred = model.forward(batch).view(-1).tolist()
            writer.writerows(zip(ids, batch_pred))

from pytorch_lightning import LightningModule

import torch
from sklearn.metrics import f1_score
from torch.utils.data import DataLoader
import torch.nn as nn

from src.datasets import PandasPickleDataset


class Model(LightningModule):
    def __init__(self, dataset: PandasPickleDataset, hidden_size=512, val_split=0.1):
        super().__init__()
        val_size = int(len(dataset) * val_split)
        train_size = len(dataset) - val_size

        self.train_set, self.val_set = torch.utils.data.random_split(dataset, [train_size, val_size])

        self.cat_embeddings = []

        embeddings_dim_sum = 0

        for levels in dataset.get_categorical_levels():
            embedding_dim = levels // 2
            embeddings_dim_sum += embedding_dim
            self.cat_embeddings.append(nn.Embedding(levels, embedding_dim))

        self.fc_1 = nn.Linear(len(dataset.continuous) + embeddings_dim_sum, hidden_size)
        self.fc_2 = nn.Linear(hidden_size, hidden_size // 2)
        self.fc_out = nn.Linear(self.fc_2.out_features, 1)

        self.batch_norm_1 = nn.BatchNorm1d(self.fc_1.out_features)
        self.batch_norm_2 = nn.BatchNorm1d(self.fc_2.out_features)

        self.input_dropout = nn.Dropout(p=0.2)
        self.input_dropout_2d = nn.Dropout2d(p=0.2)

        self.fc_1_dropout = nn.Dropout(p=0.2)
        self.fc_2_dropout = nn.Dropout(p=0.2)

    def forward(self, x):
        x_continuous, x_categorical = x

        categorical_embeddings = []
        for cat_embedding, values in zip(self.cat_embeddings, x_categorical.T):
            emb = cat_embedding(values)
            categorical_embeddings.append(
                self.input_dropout_2d(emb[None]).squeeze(dim=0)
            )

        x_continuous = self.input_dropout(x_continuous)
        x = torch.cat([x_continuous, *categorical_embeddings], 1)

        x = self.fc_1(x)
        x = self.batch_norm_1(x)
        x = torch.relu(x)
        x = self.fc_1_dropout(x)

        x = self.fc_2(x)
        x = self.batch_norm_2(x)
        x = torch.relu(x)
        x = self.fc_2_dropout(x)

        x = self.fc_out(x)
        x = torch.sigmoid(x)
        return x

    def training_step(self, batch, batch_idx):
        loss, f1 = self.step_with_f1(batch)

        tensorboard_logs = {'loss': loss, 'f1': f1}
        return {'loss': loss, 'log': tensorboard_logs}

    def validation_step(self, batch, batch_idx):
        loss, f1 = self.step_with_f1(batch)
        return {'val_loss': loss, 'val_f1': f1}

    def step_with_f1(self, batch, threshold=0.5):
        x, y = batch

        output = self.forward(x)
        loss = nn.functional.binary_cross_entropy(output.view(-1), y)

        y_pred = output > threshold
        f1 = f1_score(y, y_pred, average='macro')
        f1 = torch.tensor(f1)

        return loss, f1

    def validation_epoch_end(self, outputs):
        avg_loss = torch.stack([x['val_loss'] for x in outputs]).mean()
        avg_f1 = torch.stack([x['val_f1'] for x in outputs]).mean()

        tensorboard_logs = {'val_loss': avg_loss, 'val_f1': avg_f1}
        return {'avg_val_loss': avg_loss, 'avg_f1_loss': avg_f1, 'log': tensorboard_logs}

    def train_dataloader(self):
        return DataLoader(self.train_set, batch_size=2048)

    def val_dataloader(self):
        return DataLoader(self.val_set, batch_size=2048)

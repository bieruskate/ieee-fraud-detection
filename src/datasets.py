import json
from collections import defaultdict
from pathlib import Path
from typing import List, Tuple

import pandas as pd
import torch
from torch.utils.data import Dataset
from tqdm import tqdm


class PandasPickleDataset(Dataset):
    def __init__(self, data_file: str, feature_type_file: str, target_column: str = None,
                 normalize=True, normalization_params_file: str = None):

        self.data_file = Path(data_file)
        self.data = pd.read_pickle(data_file)
        self.continuous, self.categorical = self.get_data_types(feature_type_file)
        self.target = target_column

        if normalize:
            self.normalize_continuous_variables(normalization_params_file)

    def __getitem__(self, idx):
        x_continuous = torch.as_tensor(
            self.data.loc[idx][self.continuous].values,
            dtype=torch.float32
        )
        x_categorical = torch.as_tensor(
            self.data.loc[idx][self.categorical].values,
            dtype=torch.int64
        )
        x = (x_continuous, x_categorical)

        if self.target is None:
            return x
        else:
            y = self.data.loc[idx][self.target]
            return x, y

    def __len__(self):
        return len(self.data)

    def normalize_continuous_variables(self, normalization_params_file: str):
        if normalization_params_file is None:
            norm_params = defaultdict(dict)
        else:
            with open(normalization_params_file) as file:
                norm_params = json.load(file)

        for col in tqdm(self.continuous, desc='Continuous variables normalization'):
            if normalization_params_file is None:
                norm_params[col]['std'] = self.data[col].std()
                norm_params[col]['mean'] = self.data[col].mean()

            self.data[col] = (self.data[col] - norm_params[col]['mean']) / norm_params[col]['std']

        if normalization_params_file is None:
            with open(self.data_file.parent / f'{self.data_file.stem}-norm-params.json', mode='w') as file:
                json.dump(norm_params, file)

    def get_categorical_levels(self):
        return self.data[self.categorical].max() + 1

    @staticmethod
    def get_data_types(feature_type_file: str) -> Tuple[List[str], List[str]]:
        with open(feature_type_file) as file:
            data_types = json.load(file)
            return data_types['continuous'], data_types['categorical']

from pathlib import Path

import torch
from pytorch_lightning import Trainer

from src.datasets import PandasPickleDataset
from src.models import Model

DATA_DIR = Path('data')

if __name__ == '__main__':
    torch.manual_seed(0)

    dataset = PandasPickleDataset(
        data_file=DATA_DIR / 'train_clean.pkl',
        feature_type_file=DATA_DIR / 'variables-type.json',
        target_column='isFraud'
    )

    model = Model(dataset)
    trainer = Trainer()
    trainer.fit(model)
